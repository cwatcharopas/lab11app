json.array!(@products) do |product|
  json.extract! product, :name, :model, :released_date
  json.url product_url(product, format: :json)
end
